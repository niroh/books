@extends('layouts.app')

@section('content')
<h1>Insert New Book</h1>
<form method = 'post' action="{{action('BookController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">Which Book Would You Like To Insert?</label>
    <input type= "text" class = "form-control" name= "title">
    <label for = "title">Who Is The Author?</label>
    <input type= "text" class = "form-control" name= "author">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Insert">
</div>

</form>
@endsection